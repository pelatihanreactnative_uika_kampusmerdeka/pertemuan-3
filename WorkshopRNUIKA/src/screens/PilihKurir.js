import React, {Component} from 'react';
import {Button, Text, View} from 'react-native';
import AppContainer from '../components/AppContainer';
import TileKurir from '../components/TileKurir';
import AppStyles from '../styles/AppStyles';

export default class PilihKurir extends Component {
  constructor(props) {
    super(props);
  }
  DataKurir = [
    {
      id: 'jne',
      label: 'JNE',
      image: 'https://clodeo.com/img/logo/partner-and-courier/3x/JNE.png',
    },
    {
      id: 'pos',
      label: 'POS Indonesia',
      image:
        'https://i0.wp.com/www.desaintasik.com/wp-content/uploads/2019/04/Logo-PT-Pos-Indonesia-png-vector-corel-draw-download-desaintasik.png?resize=328%2C225',
    },
    {
      id: 'tiki',
      label: 'TIKI',
      image:
        'https://3.bp.blogspot.com/-nMmi2PtYglU/VuYpctwEyjI/AAAAAAAACEc/fH54ZbnkEPc6ynt7xscPtGs01Prv9UFeg/s1600/tiki.png',
    },
  ];

  render() {
    return (
      <AppContainer
        headerTitle="CEK ONGKIR"
        headerSubTitle="Cara Mudah Cek Ongkos Kirim">
        <View style={AppStyles.styles.container}>
          <View style={[AppStyles.styles.card]}>
            <Text
              style={[
                {color: AppStyles.color.appSecondaryColor, alignSelf: 'center'},
                AppStyles.styles.cardTitle,
              ]}>
              PILIH KURIRMU
            </Text>
            <TileKurir
              style={{marginTop: 10, alignItems: 'center'}}
              data={this.DataKurir}
              onClick={item => {
                this.props.navigation.navigate('appNav.pilihOngkir', {
                  kurirID: item.id,
                  kurirName: item.label,
                });
              }}
            />
          </View>
        </View>
      </AppContainer>
    );
  }
}
