import {Picker} from '@react-native-picker/picker';
import React, {Component} from 'react';
import {
  ActivityIndicator,
  Button,
  FlatList,
  ScrollView,
  Text,
  TextInput,
  View,
} from 'react-native';
import AppContainer from '../components/AppContainer';
import API from '../libs/API';
import AppStyles from '../styles/AppStyles';

export default class PilihOngkir extends Component {
  constructor(props) {
    super(props);
    this.props.navigation.setOptions({title: 'Updated!'});
    this.state = {
      dataProvinsiOrigin: [],
      dataCityOrigin: [],
      dataProvinsiDestination: [],
      dataCityDestination: [],
      selectedProvOrigin: null,
      selectedProvDestination: null,
      origin: null,
      destination: null,
      courier: this.props.route.params.kurirID,
      courierName: this.props.route.params.kurirName,
      weight: null,
      loading: true,
      result: null,
    };
    this.getDataProvinsi = this.getDataProvinsi.bind(this);
    this.getDataCityByProv = this.getDataCityByProv.bind(this);
    this.onChangeProv = this.onChangeProv.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    this.getDataProvinsi();
  }

  getDataProvinsi = () => {
    this.setState({loading: true});
    API('/province')
      .then(res => {
        this.setState({
          dataProvinsiDestination: res.data.rajaongkir.results,
          dataProvinsiOrigin: res.data.rajaongkir.results,
        });
      })
      .catch(err => {
        alert(err);
      })
      .finally(() => {
        this.setState({loading: false});
      });
  };

  getDataCityByProv = (provID, stateID) => {
    if (provID == null || provID == '') {
      this.setState({[stateID]: []});
      return;
    }
    this.setState({loading: true});
    API('/city?province=' + provID)
      .then(res => {
        this.setState({
          [stateID]: res.data.rajaongkir.results,
        });
      })
      .catch(err => {
        alert(err);
      })
      .finally(() => {
        this.setState({loading: false});
      });
  };

  onChangeProv = (val, stateID, stateCityID) => {
    this.setState({[stateID]: val}, () =>
      this.getDataCityByProv(val, stateCityID),
    );
  };

  onSubmit = () => {
    if (
      this.state.origin &&
      this.state.destination &&
      this.state.courier &&
      this.state.weight
    ) {
      let fd = new FormData();
      fd.append('origin', this.state.origin);
      fd.append('destination', this.state.destination);
      fd.append('courier', this.state.courier);
      fd.append('weight', this.state.weight);
      this.setState({loading: true});
      API('/cost', 'post', fd)
        .then(res => {
          let result = res.data.rajaongkir.results;
          result[0].costs = [
            ...result[0].costs,
            ...result[0].costs,
            ...result[0].costs,
          ];
          this.setState({result});
        })
        .catch(err =>
          alert('Tidak ada data ongkos kirim, coba dengan parameter lain'),
        )
        .finally(() => this.setState({loading: false}));
    } else {
      alert('Parameter tidak valid, silahkan cek kembali');
    }
  };

  render() {
    return (
      <AppContainer
        headerTitle={`ONGKOS KIRIM ${this.state.courierName.toUpperCase()}`}
        headerSubTitle={`Cari Ongkos kirim ${this.state.courierName.toUpperCase()} mudah dan cepat`}>
        {this.state.loading ? (
          <View style={{alignSelf: 'center'}}>
            <ActivityIndicator
              color={AppStyles.color.appPrimaryColor}
              size="large"
              style={{flex: 1}}
            />
          </View>
        ) : (
          <View style={AppStyles.styles.container}>
            {this.state.result == null && (
              <View style={[AppStyles.styles.card]}>
                <Text
                  style={[
                    AppStyles.styles.cardTitle,
                    {color: AppStyles.color.appPrimaryColor},
                  ]}>
                  Pilih Asal dan Tujuan
                </Text>
                <View style={{marginTop: 10}}>
                  <View>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        marginLeft: 15,
                      }}>
                      Asal
                    </Text>
                    <Picker
                      key="provOrigin"
                      selectedValue={this.state.selectedProvOrigin}
                      onValueChange={val =>
                        this.onChangeProv(
                          val,
                          'selectedProvOrigin',
                          'dataCityOrigin',
                        )
                      }>
                      <Picker.Item value={null} label="Pilih Provinsi" />
                      {this.state.dataProvinsiOrigin.map(prov => (
                        <Picker.Item
                          key={`provOriginOpt-${prov.province_id}`}
                          label={prov.province}
                          value={prov.province_id}
                        />
                      ))}
                    </Picker>
                    <Picker
                      key="cityOrigin"
                      selectedValue={this.state.origin}
                      onValueChange={val => this.setState({origin: val})}>
                      <Picker.Item value={null} label="Pilih Kab/Kota" />
                      {this.state.dataCityOrigin.map(city => (
                        <Picker.Item
                          key={`cityOriginOpt-${city.city_id}`}
                          label={city.city_name}
                          value={city.city_id}
                        />
                      ))}
                    </Picker>
                  </View>
                  <View
                    style={{
                      borderTopColor: AppStyles.color.appSecondaryColor,
                      borderTopWidth: 1,
                      paddingTop: 10,
                    }}>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        marginLeft: 15,
                      }}>
                      Tujuan
                    </Text>
                    <Picker
                      key="provDest"
                      selectedValue={this.state.selectedProvDestination}
                      onValueChange={val =>
                        this.onChangeProv(
                          val,
                          'selectedProvDestination',
                          'dataCityDestination',
                        )
                      }>
                      <Picker.Item value={null} label="Pilih Provinsi" />
                      {this.state.dataProvinsiDestination.map(prov => (
                        <Picker.Item
                          key={`provDestOpt-${prov.province_id}`}
                          label={prov.province}
                          value={prov.province_id}
                        />
                      ))}
                    </Picker>

                    <Picker
                      key="cityDest"
                      selectedValue={this.state.destination}
                      onValueChange={val => this.setState({destination: val})}>
                      <Picker.Item value={null} label="Pilih Kab/Kota" />
                      {this.state.dataCityDestination.map(city => (
                        <Picker.Item
                          key={`cityDestOpt-${city.city_id}`}
                          label={city.city_name}
                          value={city.city_id}
                        />
                      ))}
                    </Picker>
                  </View>
                </View>

                <View style={{flexDirection: 'row'}}>
                  <TextInput
                    keyboardType="numeric"
                    value={this.state.weight}
                    onChangeText={val => this.setState({weight: val})}
                    style={[AppStyles.styles.inputText, {flex: 1}]}
                    placeholder="Masukan Berat Barang"
                  />
                  <Text style={{alignSelf: 'center', fontWeight: 'bold'}}>
                    Gram
                  </Text>
                </View>
                <Button
                  color={AppStyles.color.appSecondaryColor}
                  title="Cari Ongkir"
                  onPress={this.onSubmit}
                />
                <View style={{marginTop: 10}}>
                  <Button
                    color={AppStyles.color.appPrimaryColor}
                    title="Pilih Kurir"
                    onPress={() => this.props.navigation.goBack()}
                  />
                </View>
              </View>
            )}

            {this.state.result &&
              this.state.result.length > 0 &&
              this.state.result[0].costs.length > 0 && (
                <View>
                  <View style={[AppStyles.styles.card]}>
                    <Text
                      style={[
                        AppStyles.styles.cardTitle,
                        {fontSize: 14, color: AppStyles.color.appPrimaryColor},
                      ]}>
                      Dari{' '}
                      {
                        this.state.dataCityOrigin.find(
                          item => item.city_id === this.state.origin,
                        ).city_name
                      }
                      (
                      {
                        this.state.dataProvinsiOrigin.find(
                          item =>
                            item.province_id === this.state.selectedProvOrigin,
                        ).province
                      }
                      )
                    </Text>

                    <Text
                      style={[
                        AppStyles.styles.cardTitle,
                        {fontSize: 14, color: AppStyles.color.appPrimaryColor},
                      ]}>
                      Ke{' '}
                      {
                        this.state.dataCityDestination.find(
                          item => item.city_id === this.state.destination,
                        ).city_name
                      }
                      (
                      {
                        this.state.dataProvinsiDestination.find(
                          item =>
                            item.province_id ===
                            this.state.selectedProvDestination,
                        ).province
                      }
                      )
                    </Text>
                    <Text
                      style={[
                        AppStyles.styles.cardTitle,
                        {fontSize: 14, color: AppStyles.color.appPrimaryColor},
                      ]}>
                      Berat Barang {this.state.weight} Gram
                    </Text>
                    <View style={{marginVertical: 5}}>
                      <Button
                        title="Cari Ulang Ongkir"
                        color="teal"
                        onPress={() => this.setState({result: null})}
                      />
                    </View>
                    <FlatList
                      ListFooterComponent={<View />}
                      ListFooterComponentStyle={{height: 100}}
                      contentContainerStyle={{paddingBottom: 100}}
                      data={this.state.result[0].costs}
                      renderItem={({item}) => (
                        <View
                          key={`resultItem${item.service}`}
                          style={[
                            AppStyles.styles.card,
                            {
                              marginVertical: 8,
                              elevation: 0,
                              borderRadius: 5,
                              borderColor: AppStyles.color.appSecondaryColor,
                              borderWidth: 1,
                            },
                          ]}>
                          <Text style={[AppStyles.styles.cardTitle]}>
                            {item.service}
                          </Text>
                          <Text>{item.description}</Text>
                          <View
                            style={{
                              borderTopColor: 'grey',
                              borderTopWidth: 0.5,
                              marginTop: 5,
                            }}>
                            {item.cost.map((c, i) => (
                              <View
                                key={`constItem${item.service}${i}`}
                                style={{
                                  borderBottomColor: 'grey',
                                  borderBottomWidth: 0.5,
                                }}>
                                <View style={{flexDirection: 'row'}}>
                                  <View style={{flex: 1}}>
                                    <Text style={{fontSize: 11}}>Harga</Text>
                                    <Text
                                      style={{
                                        fontSize: 16,
                                        fontWeight: 'bold',
                                        color:
                                          AppStyles.color.appSecondaryColor,
                                      }}>
                                      Rp{Number(c.value).toLocaleString('id')}
                                    </Text>
                                  </View>
                                  <View style={{flex: 1}}>
                                    <Text style={{fontSize: 11}}>Sampai</Text>
                                    <Text
                                      style={{
                                        fontSize: 16,
                                        color:
                                          AppStyles.color.appSecondaryColor,
                                      }}>
                                      {c.etd}
                                    </Text>
                                  </View>
                                </View>
                                <View style={{flexDirection: 'row'}}>
                                  <View style={{flex: 1}}>
                                    <Text style={{fontSize: 11}}>Catatan</Text>
                                    <Text
                                      style={{
                                        fontSize: 16,
                                        color:
                                          AppStyles.color.appSecondaryColor,
                                      }}>
                                      {c.note}
                                    </Text>
                                  </View>
                                </View>
                              </View>
                            ))}
                          </View>
                        </View>
                      )}
                      keyExtractor={(item, index) => index}
                    />
                  </View>
                </View>
              )}
          </View>
        )}
      </AppContainer>
    );
  }
}
