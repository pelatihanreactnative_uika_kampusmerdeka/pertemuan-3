import axios from 'axios';

export default API = (url = '', method = 'get', data = null, headers = null) =>
  axios({
    url: `https://api.rajaongkir.com/starter${url}`,
    data,
    method,
    headers: {
      key: '4a708a852cb9e4351efadc9e7e2a6e5f',
      'Content-Type':
        method != 'get'
          ? 'multipart/form-data;boundary=4a708a852cb9e4351efadc9e7e2a6e5f'
          : '',
      ...headers,
    },
  });
