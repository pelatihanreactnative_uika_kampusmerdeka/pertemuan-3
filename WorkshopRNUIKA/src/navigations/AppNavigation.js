// Dependencies
// yarn add @react-navigation/native
// yarn add react-native-screens react-native-safe-area-context
// yarn add @react-navigation/native-stack
import React, {Component} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import PilihKurir from '../screens/PilihKurir';
import PilihOngkir from '../screens/PilihOngkir';

const Stack = createNativeStackNavigator();
export default class AppNavigation extends Component {
  render() {
    return (
      <Stack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName="appNav.pilihKurir">
        <Stack.Screen name="appNav.pilihKurir" component={PilihKurir} />
        <Stack.Screen name="appNav.pilihOngkir" component={PilihOngkir} />
      </Stack.Navigator>
    );
  }
}
