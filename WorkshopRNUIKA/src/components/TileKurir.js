import React, {Component} from 'react';
import {
  View,
  Image,
  Text,
  FlatList,
  TouchableNativeFeedback,
  StyleSheet,
} from 'react-native';
import AppStyles from '../styles/AppStyles';

export default class TileKurir extends Component {
  styles = StyleSheet.create({
    tileTitle: {
      fontSize: 14,
      fontWeight: 'bold',
      color: '#6f7173',
    },
    tileImage: {
      width: 70,
      height: 70,
    },
    tileImageContainer: {
      borderRadius: 50,
      borderColor: AppStyles.color.appSecondaryColor,
      borderWidth: 2,
      padding: 3,
    },
    tileContainer: {
      alignItems: 'center',
      margin: 10,
    },
  });

  ItemView = item => (
    <TouchableNativeFeedback
      onPress={() => {
        this.props.onClick && this.props.onClick(item);
      }}>
      <View style={this.styles.tileContainer}>
        <View style={this.styles.tileImageContainer}>
          <Image
            resizeMode="contain"
            source={{uri: item.image}}
            style={this.styles.tileImage}
          />
        </View>
        <Text style={this.styles.tileTitle}>{item.label}</Text>
      </View>
    </TouchableNativeFeedback>
  );

  render() {
    return (
      <View style={this.props.style ?? {}}>
        <FlatList
          data={this.props.data ?? []}
          renderItem={({item}) => this.ItemView(item)}
          numColumns={3}
          keyExtractor={(item, index) => index}
        />
      </View>
    );
  }
}
