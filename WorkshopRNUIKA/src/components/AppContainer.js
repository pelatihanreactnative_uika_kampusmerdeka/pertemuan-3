import React, {Component} from 'react';
import {SafeAreaView, Text, StatusBar, View} from 'react-native';
import AppStyles from '../styles/AppStyles';

export default class AppContainer extends Component {
  render() {
    return (
      <SafeAreaView>
        <View>
          <StatusBar backgroundColor={AppStyles.color.appPrimaryColor} />
          <View
            style={{
              backgroundColor: AppStyles.color.appPrimaryColor,
              height: 150,
              position: 'absolute',
              top: 0,
              left: 0,
              right: 0,
              borderBottomLeftRadius: 100,
            }}
          />
          <View style={AppStyles.styles.container}>
            <Text style={AppStyles.styles.headerTitle}>
              {this.props.headerTitle ?? 'HEADER TITLE'}
            </Text>
            {this.props.headerSubTitle && (
              <Text style={AppStyles.styles.headerSubTitle}>
                {this.props.headerSubTitle}
              </Text>
            )}
          </View>
          {this.props.children}
        </View>
      </SafeAreaView>
    );
  }
}
