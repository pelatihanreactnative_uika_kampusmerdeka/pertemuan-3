import {StyleSheet} from 'react-native';

const AppStyles = {
  color: {
    appPrimaryColor: 'teal',
    appSecondaryColor: 'darkorange',
  },
  styles: StyleSheet.create({
    container: {
      padding: 16,
    },
    headerTitle: {
      fontSize: 20,
      fontWeight: 'bold',
      color: 'white',
    },
    headerSubTitle: {
      fontSize: 14,
      color: 'white',
    },
    card: {
      borderRadius: 25,
      backgroundColor: 'rgba(255,255,255,1)',
      padding: 16,
      elevation: 5,
    },
    cardTitle: {
      fontSize: 18,
      fontWeight: 'bold',
    },
    inputText: {
      width: '100%',
      borderBottomColor: 'teal',
      borderBottomWidth: 1,
      borderRadius: 5,
      padding: 10,
      marginVertical: 5,
    },
  }),
};
export default AppStyles;
