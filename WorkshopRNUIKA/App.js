import {NavigationContainer} from '@react-navigation/native';
import React, {Component} from 'react';
import AppNavigation from './src/navigations/AppNavigation';
class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <AppNavigation />
      </NavigationContainer>
    );
  }
}

export default App;
